<?php setcookie("filedbTest", "1", time() + 180); ?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>FileDB Login</title>
<link rel="stylesheet" type="text/css" href="assets/css/default.css">
</head>
<body>
<h1>File Database Login</h1>
<div>
<form method="post" action="auth.php">
<table>
<tr>
<td class="tdLabel"><label>Username:</label></td>
<td><input type="text" name="username" maxlength="64"></td>
</tr>
<tr style="height:10px"><td colspan="2"></td></tr>
<tr>
<td class="tdLabel"><label>Password:</label></td>
<td><input type="password" name="password"></td>
</tr>
<tr style="height:10px"><td colspan="2"></td></tr>
<tr>
<td colspan="2"><input type="submit" name="login" class="InputButton" value="Login"></td>
</tr>
<tr style="height:7px"><td colspan="2"></td></tr>
<tr>
<td colspan="2"><input type="submit" name="validSession" class="InputButton" value="Is session Valid?"></td>
</tr>
</table>
</form>
<br>
<?php

    /* WHEN PASSWORD IS ENTERED MAKE IT INTO A COOKIE */
    if(isset($_POST["login"]))
    {
        /* CHECK IF COOKIES ARE ENABLED */
    	if(count($_COOKIE) < 1){
            die("<h1>Cookies don't appear to be enabled</h1><h1>Enable and try again</h1>");
        }

        require_once "util.php";
        $userAuth = new util;

        if ($userAuth->login($_POST["username"], $_POST["password"]) == TRUE) {
            printOptions();
        }
    }

    /* BUTTON CHECKS TO SEE IF COOKIE IS VALID */
    if(isset($_POST["validSession"]))
    {
        if (isset($_COOKIE["filedbAuth"]))
        {
            echo "<h3>Session <b>is</b> set</h3>";
            printOptions();
        }
        else
        {
            echo "<h3>Session <b>is NOT</b> set</h3>";
        }
    }

    function printOptions(){
        echo "<br><br>";
        echo "<a href='file.php'>Files</a>";
        echo "<br><br>";
        echo "<a href='settings.php'>Settings</a>";
    }
    ?>
</div>
</body>
</html>