<?php

require_once 'util.php';
require_once "dbconn.php";
$userAuth = new util;

$userDetails = $userAuth->validSession();
$userId = $userDetails[1];
$doProcessing = false;

if (isset($_GET['id']) && isset($_GET['fid']))
{
    $doProcessing = true;
    $fileId = $_GET['id'];
    $folderId = $_GET['fid'];
    $paging = $userAuth->getParam("slideshow.paging", "0");
    $pageSize = $userAuth->getParam("slideshow.pagesize", "20");
    $piclink = $userAuth->getParam("slideshow.piclink", "0");
}

?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<title>Slideshow</title>
<script src="assets/js/jquery-3.7.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/css/slideshow.css">

</head>
<body>
<?php

if ($doProcessing)
{

$sql = "SELECT C.`id`, C.`name` ";
$sql .= "FROM `objectHierarchy` A ";
$sql .= "INNER JOIN `objectHierarchy` B ";
$sql .= "ON A.`parentComponent` = B.`parentComponent` ";
$sql .= "AND A.`parentInstance` = B.`parentInstance` ";
$sql .= "INNER JOIN `file` C ";
$sql .= "ON B.`childComponent` = 1 AND B.`childInstance` = C.id ";
$sql .= "WHERE A.`parentComponent` = 2 AND A.`parentInstance` = $folderId ";
$sql .= "AND A.`childComponent` = 1 AND A.`childInstance` = $fileId AND C.`userId` = $userId ";
$sql .= "AND C.`type` IN (SELECT `mimeType` FROM `fileType` WHERE `type` = 'image') ";
if ($paging > 0) { $sql .= "AND B.`sort` >= A.`sort` "; }
$sql .= "ORDER BY C.`name` ";
if ($paging > 0) { $sql .= "LIMIT $pageSize "; }
$sql .= ";";


if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

$startIndex = 1;
$endIndex = 0;
$currentIndex = 500;

$linkSet = array();

while ($dataRow = $result->fetch_row())
{
    $linkRow = array();
    $linkRow['id'] = $dataRow[0];
    $linkRow['name'] = $dataRow[1];
    array_push($linkSet, $linkRow);
    $endIndex++;
    if ($dataRow[0] == $fileId) { $currentIndex = $endIndex; }
}

}

?>

<script type="text/javascript">

    var slideIndex = <?php echo $currentIndex; ?>;

    $( document ).ready(function() {
        showSlides(slideIndex);
    });

    // Next/previous controls
    function plusSlides(n) {
      showSlides(slideIndex += n);
    }

    // Thumbnail image controls
    function currentSlide(n) {
      showSlides(slideIndex = n);
    }

    function showSlides(n) {
      var i;
      var slides = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("dot");
      if (n > slides.length) {slideIndex = 1}
      if (n < 1) {slideIndex = slides.length}
      for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
      }
      for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
      }
      slides[slideIndex-1].style.display = "block";
      dots[slideIndex-1].className += " active";
    }
</script>

<!-- Slideshow container -->

<div class="slideshow-container">

<?php

$countIndex = 1;

foreach ($linkSet as $row)
{
    echo "<div class=\"mySlides fade\">\n";
    echo "\t<div class=\"numbertext\">$countIndex / $endIndex</div>\n";
    if ($piclink > 0) { echo "\t<a href=\"fileget.php?id=" . $row['id'] . "\">"; }
    echo "\t<img src=\"fileget.php?id=" . $row['id'] . "\" alt=\"" . $row['name'] . "\"  class=\"center-fit\">\n";
    if ($piclink > 0) { echo "\t</a>"; }
    echo "\t<div class=\"text\">" . $row['name'] . "</div>\n";
    echo "</div>\n\n";
    $countIndex++;
}

?>

  <!-- Next and previous buttons -->
  <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
  <a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>
</body>
</html>