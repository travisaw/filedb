<?php
/* CHECK THAT THIS FILE IS CALLED FROM A VALID PAGE */
if(basename($_SERVER['PHP_SELF']) !== "settings.php") {
    die("invalid call");
}
?>
<hr>
<h2>Admin Functions</h2>
<h3>Update User Details</h3>
<script type="text/javascript">
function setPasswordAdmin(f){var b="";$("input:radio").each(function(){if($(this).is(":checked")){b=$(this).val()}});if(b=="new"){var d=$("#txtUserName").val();if(d==""){$("#lblPasswordStatusAdmin").text("Username Is Blank");return}}else{if(b=="existing"){var d=$("#ddlUser").val();if(d==""){$("#lblPasswordStatusAdmin").text("Select User");return}}else{$("#lblPasswordStatusAdmin").text("Select New or Existing");return}}var a=$("#txtPasswordUser").val();var c=$("#txtPasswordUserConf").val();if(a!=c){$("#lblPasswordStatusAdmin").text("Passwords Do Not Match");setTimeout(function(){$("#lblPasswordStatusAdmin").text("")},3000);return}var e="action=adminpass";e+="&username="+d;e+="&password="+a;e+="&token="+f;$.ajax({type:"GET",url:"ajax.php",data:e,success:function(g){$("#lblPasswordStatusAdmin").text(g);if(g=="Password Updated"){$("#lblPasswordStatusAdmin").addClass("txtUpdatePass")}else{$("#lblPasswordStatusAdmin").addClass("txtUpdateFail")}setTimeout(function(){$("#lblPasswordStatusAdmin").text("")},3000)},error:function(){$("#lblPasswordStatusAdmin").text("Call Failed");$("#lblPasswordStatusAdmin").addClass("txtUpdateFail");setTimeout(function(){$("#lblPasswordStatusAdmin").text("")},3000)}});$("#txtUserName").val("");$("#ddlUser").val("");$("#txtPasswordUser").val("");$("#txtPasswordUserConf").val("")}function updateParam(b,h){var e="#param"+b;var d="#txtParam"+b;var a="#paramStatus"+b;var c=$(e).text();var g=$(d).val();var f="action=param";f+="&paramname="+c;f+="&paramvalue="+g;f+="&token="+h;$.ajax({type:"GET",url:"ajax.php",data:f,success:function(i){$(a).text(i);if(i=="Updated"){$(a).addClass("txtUpdatePass")}else{$(a).addClass("txtUpdateFail")}setTimeout(function(){$(a).text("")},3000)},error:function(){$(a).text("Call Failed");$(a).addClass("txtUpdateFail");setTimeout(function(){$(a).text("")},3000)}})};
</script>
<div>
<input type="radio" name="rdoUserState" value="new"><label>New User</label><br>
<input type="radio" name="rdoUserState" value="existing"><label>Existing User</label><br><br>
<table>
<tr>
<td><label>New User: </label></td>
<td><input type="text" id="txtUserName" maxlength='64'></td>
</tr>
<tr>
<td colspan="2"><label>OR</label></td>
</tr>
<tr>
<td><label>Existing User:</label></td>
<td>
<?php
/* Get list of users */
$users = $userAuth->getUserList();
/* Put into a drop down list */
echo "<select id='ddlUser'>\n";
echo "\t\t\t\t<option value=''></option>\n";
foreach ($users as $user)
{
echo "\t\t\t\t<option value='$user'>$user</option>\n";
}
echo "\t\t\t</select>\n";
?>
</td>
</tr>
<tr style="height:10px"><td colspan="2"></td></tr>
<tr>
<td><label>Password: </label></td>
<td><input type="password" id="txtPasswordUser"></td>
</tr>
<tr>
<td><label>Confirm: </label></td>
<td><input type="password" id="txtPasswordUserConf"></td>
</tr>
<tr style="height:10px"><td colspan="2"></td></tr>
<tr>
<td colspan="2"><input type="submit" name="updateUserPW" class="InputButton" value="Submit Password" onClick='setPasswordAdmin(<?php echo '"' . $userKey . '"'; ?>)'></td>
</tr>
<tr>
<td colspan="2"><label id="lblPasswordStatusAdmin"></label></td>
</tr>
</table>
<h3>System Parameters</h3>
<?php $userAuth->getParamList($userKey)?>
<br>
</div>
