<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>FileDB Settings</title>
<link rel="stylesheet" type="text/css" href="assets/css/default.css">
<script src="assets/js/jquery-3.7.1.min.js"></script>
</head>
<body>
<script>
function setUserPassword(c,e){var a=$("#txtPassword").val();var b=$("#txtPasswordConf").val();if(a!=b){$("#lblPasswordStatus").text("Passwords Do Not Match");$("#lblPasswordStatus").addClass("txtUpdateFail");setTimeout(function(){$("#lblPasswordStatus").text("")},3000)}else{var d="action=userpass";d+="&username="+c;d+="&password="+a;d+="&token="+e;$.ajax({type:"GET",url:"ajax.php",data:d,success:function(f){$("#lblPasswordStatus").text(f);if(f=="Password Updated"){$("#lblPasswordStatus").addClass("txtUpdatePass")}else{$("#lblPasswordStatus").addClass("txtUpdateFail")}setTimeout(function(){$("#lblPasswordStatus").text("")},3000)},error:function(){$("#lblPasswordStatus").text("Call Failed");$("#lblPasswordStatus").addClass("txtUpdateFail");setTimeout(function(){$("#lblPasswordStatus").text("")},3000)}});$("#txtPassword").val("");$("#txtPasswordConf").val("")}}function setUserColumns(b,d){var a=$("#txtColWidth").val();var c="action=usercol";c+="&username="+b;c+="&colwidth="+a;c+="&token="+d;$.ajax({type:"GET",url:"ajax.php",data:c,success:function(e){$("#lblColumnStatus").text(e);if(e=="Width Updated"){$("#lblColumnStatus").addClass("txtUpdatePass")}else{$("#lblColumnStatus").addClass("txtUpdateFail")}setTimeout(function(){$("#lblColumnStatus").text("")},3000)},error:function(){$("#lblColumnStatus").text("Call Failed");$("#lblColumnStatus").addClass("txtUpdateFail");setTimeout(function(){$("#lblColumnStatus").text("")},3000)}})};
</script>
<?php
/* VALIDATE USER SESSION */
require_once "util.php";
$userAuth = new util;

$userDetails = $userAuth->validSession();
$userName = $userDetails[0];
$userId = $userDetails[1];
$columns = $userDetails[2];
$userKey = $userDetails[3];
?>
<div>
<h2>Change Password</h2>
<input type="password" id="txtPassword"><br><br>
<input type="password" id="txtPasswordConf"><br><br>
<input type="submit" id="updatePW" class="InputButton" value="Update Password" onClick='setUserPassword(<?php echo '"' . $userName . '", "' . $userKey . '"'; ?>)'>&nbsp;&nbsp;
<label id='lblPasswordStatus'></label><br><br>
<h2>Display Columns</h2>
<input type="text" id="txtColWidth" maxlength="4" size="4" value='<?php echo $columns; ?>'>&nbsp;&nbsp;
<label id="lblColumnStatus"></label><br>
<input type="submit" name="updateColWidth" class="InputButton" value='Update Column Width' onClick='setUserColumns(<?php echo '"' . $userName . '", "' . $userKey . '"'; ?>)'><br><br>
</div>
<?php

/* If user is admin load admin console */
if ($userAuth->isValidAdmin($userKey) == true) {
    include_once "admin.php";
}
?>
<a href="file.php">Back to Files</a>
<br><br>
</body>
</html>