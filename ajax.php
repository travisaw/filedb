<?php

    require_once "util.php";

	// http://server.local/webdev/filedb/ajax.php?paramname=test1&paramvalue=34
	// http://server.local/webdev/filedb/ajax.php?action=usercol&username=admin&colwidth=34

    if (empty($_GET["token"])) {
        echo "Invalid Token";
        return;
    }

    if ($_GET["action"] == "param") { updateParam(); }
    else if ($_GET["action"] == "usercol") { updateUserColumn(); }
    else if ($_GET["action"] == "userpass") { updateUserPassword(); }
    else if ($_GET["action"] == "adminpass") { updateAdminPassword(); }
    else if ($_GET["action"] == "archivefolder") { archiveFolder(); }
    else { echo "Unknown Action"; }

    /*------------------------------------------------------------------------------------------------*/
    /* USER FUNCTIONS */
    /*------------------------------------------------------------------------------------------------*/

    function updateUserPassword() {
    
        /* CHECK IF NEEDED VARIABLES ARE PASSED */
        if (!isset($_GET["username"]) || !isset($_GET["password"])) {
            echo "Invalid Parameters";
            return;
        }
    
        $util = new util;

        if ($util->isValidUser($_GET["token"], $_GET["username"])) {
            $util->setPassword($_GET["username"], $_GET["password"], false);
        }
        else {
            echo "Invalid Action";
        }
    }

    function updateUserColumn() {

        /* CHECK IF NEEDED VARIABLES ARE PASSED */
        if (!isset($_GET["username"]) || !isset($_GET["colwidth"])) {
            echo "Invalid Parameters";
            return;
        }

        $util = new util;

        if ($util->isValidUser($_GET["token"], $_GET["username"]) == true) {
            $util->setColumnWidth($_GET["username"], $_GET["colwidth"]);
        }
        else {
            echo "Invalid Action";
        }
    }

    /*------------------------------------------------------------------------------------------------*/
    /* ADMIN FUNCTIONS */
    /*------------------------------------------------------------------------------------------------*/

    function updateAdminPassword() {

        /* CHECK IF NEEDED VARIABLES ARE PASSED */
        if (!isset($_GET["username"]) || !isset($_GET["password"])) {
            echo "Invalid Parameters";
            return;
        }

        $util = new util;

        if ($util->isValidAdmin($_GET["token"]) == true) {
            $util->setPassword($_GET["username"], $_GET["password"], true);
        }
        else {
            echo "Invalid Action";
        }
    }

    function updateParam() {

        /* CHECK IF NEEDED VARIABLES ARE PASSED */
        if (!isset($_GET["paramname"]) || !isset($_GET["paramvalue"])) {
            echo "Invalid Parameters";
            return;
        }

        $util = new util;
    
        $paramName = $_GET["paramname"];
        $paramValue = $_GET["paramvalue"];
        
        if ($util->isValidAdmin($_GET["token"]) == true) {
            $util->setParam($paramName, $paramValue);
        }
        else {
            echo "Invalid Action";
        }
	}

    /*------------------------------------------------------------------------------------------------*/
    /* FOLDER ARCHIVE */
    /*------------------------------------------------------------------------------------------------*/
    function archiveFolder() {
        /* CHECK IF NEEDED VARIABLES ARE PASSED */
        if (!isset($_GET["folderid"]) || !isset($_GET["userid"])) {
            echo "Invalid Parameters";
            return;
        }
        $util = new util;
        $util->archiveFolder($_GET["folderid"], $_GET["userid"]);
        echo "Got it";
	}

?>