<?php

class util
{

function login ($userName, $password)
{
    require 'dbconn.php';

    $authenticated = false;
    $timestamp = time();
    $datestamp = date('Y/m/d H:i:s', $timestamp);

    $userName = trim($userName);
    $password = trim($password);

    $authMode = $this->getParam("authLog.enabled", "0");

    $pwSql = "SELECT password FROM users WHERE userName = '$userName';";

    /* Execute Query */
    if (!$resultAuth = $con->query($pwSql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    /* Fetch Results and build cookie */
    if ($resultAuth->num_rows > 0) {
        $rowAuth = $resultAuth->fetch_row();
        $cryptPW = $rowAuth[0];

        /* If auth success set session key and get timeout */
        if(sha1($password) == $cryptPW) {

            $hashCookie = sha1($timestamp . $userName . $cryptPW);

            /* Update Users Session Key */
            $keySql = "UPDATE users SET sessionKey = '$hashCookie', sessionStamp = '$datestamp' WHERE userName = '$userName';";
            if (!$con->query($keySql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

            /* Get Session Timeout */
            $timeout = $this->getParam("sessionTimeout", 600);

            setcookie("filedbAuth", $hashCookie, time() + $timeout);
            if ($authMode > 0) { $this->authLog(3, $userName, 0); }
            echo "<label><b>Login Successful</b></label>";

        }
        else {
            echo "<label><b>Incorrect Password</b></label>";
            if ($authMode > 0) { $this->authLog(2, $userName, 0); }
            return $authenticated;
        }
    }
    else {
        echo "<label><b>Invalid User</b></label>";
        if ($authMode > 0) { $this->authLog(1, $userName, 0); }
        return $authenticated;
    }

    $con->close();
    // RUN SP TO LOG AUTHNTICATION RESULTS

    return true;
}

/* Set users password */
function setPassword($userName, $password, $adminMode = false)
{
    require 'dbconn.php';

    $newPw = sha1($password);

    $sqlPw = "CALL `insertUser`('$userName', '$newPw');";

    if (!$con->query($sqlPw)) {
        echo "CALL failed: (" . $con->errno . ") " . $con->error;
    }
    else {
        echo "Password Updated";
        if ($adminMode = false) { setcookie("filedbAuth", "", time() - 1); }
    }

    $con->close();
}

/* Set users column width */
function setColumnWidth($userName, $width)
{
    require 'dbconn.php';

    $sqlCol = "UPDATE users SET viewColumns = $width WHERE userName = '$userName';";

    if (!$con->query($sqlCol)) {
        echo "CALL failed: (" . $con->errno . ") " . $con->error . "<br><br>";
    }
    else {
        echo "Width Updated";
    }

    $con->close();
}

/* Get parameter from PARAM table */
function getParam($paramName, $defaultValue = "")
{
    require 'dbconn.php';

    $paramValue = $defaultValue;

    $sql = "SELECT `valueString` FROM `param` WHERE `valueKey` = '$paramName';";
    if (!$result = $con->query($sql)) { echo "Param Query Failed"; }

    if ($result->num_rows > 0) {
        $row = $result->fetch_row();
        $paramValue = $row[0];
    }

    $con->close();
    return $paramValue;
}

/* Updates parameter in PARAM table */
function setParam($paramName, $paramValue)
{
    require 'dbconn.php';

    $sql = "UPDATE `param` SET `valueString` = '$paramValue' WHERE `valueKey` = '$paramName';";
    if (!$result = $con->query($sql)) { echo "Update Failed"; } else { echo "Updated"; }

    $con->close();
}

/* Checks to make sure session is valid */
function validSession()
{
    /* If cookie doesn't exist then redirect to auth.php. */
    if (!isset($_COOKIE['filedbAuth']))
    {
        session_destroy();
        header("Location: auth.php");
    }

    /* If cookie is valid make sure key is valid and get username. */
    require 'dbconn.php';

    $authKey = $_COOKIE['filedbAuth'];
    $sql = "SELECT userName, id, viewColumns, sessionKey FROM users WHERE sessionKey = '$authKey'";

    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows > 0) {
        $row = $result->fetch_row();
        $userDetails[0] = $row[0];
        $userDetails[1] = $row[1];
        $userDetails[2] = $row[2];
        $userDetails[3] = $row[3];
        return $userDetails;
    }
    else {
        session_destroy();
        header("Location: auth.php");
    }
}

/* Validate user has permission to view file. */
function userPermission($userId, $fileId)
{
    require 'dbconn.php';

    $userValid = FALSE;
    $fileSql = "SELECT userId FROM file WHERE id = $fileId;";
    if (!$result = $con->query($fileSql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows > 0) {
        $row = $result->fetch_row();
        if ($row[0] == $userId) { $userValid = TRUE; }
    }

    $con->close();

    return $userValid;
}

/* Log authentication results. */
function authLog($authType, $userName, $fileId) {
    require 'dbconn.php';

    $authSql = "CALL insertAuth ('" . $_SERVER['REMOTE_ADDR'] . "', '$userName', $authType, $fileId);";

    if (!$result = $con->query($authSql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    $con->close();
}

/* Return list of all users. */
function getUserList()
{
    require 'dbconn.php';

    $sql = "SELECT `userName` FROM `users` ORDER BY userName;";
    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    $rowCount = 0;
    if ($result->num_rows > 0) {
        while($row = $result->fetch_row())
        {
            $userList[$rowCount] = $row[0];
            $rowCount++;
        }
    }

    $con->close();

    return $userList;
}

/* Return list of all parameters from param table. HTML formatted. */
function getParamList($userKey)
{
    require 'dbconn.php';

    $sql = "SELECT `valueKey`, `valueString` FROM `param` ORDER BY valueKey;";
    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    $rowCount = 1;
    if ($result->num_rows > 0) {
        echo "<table id='tblParams'>\n";
        while($row = $result->fetch_row())
        {
            echo "\t<tr>\n";
            echo "\t\t<td>&nbsp;<label id='param$rowCount'>$row[0]</label>&nbsp;</td>\n";
            echo "\t\t<td>&nbsp;<input type='text' id='txtParam$rowCount' value='$row[1]' onChange='updateParam($rowCount, \"$userKey\");'>&nbsp;</td>\n";
            echo "\t\t<td><label id='paramStatus$rowCount'></label></td>\n";
            echo "\t</tr>\n";
            $rowCount++;
        }
        echo "</table>\n";
    }
}

/* Verify user has permission to take action. */
function isValidUser($sessionKey, $userName)
{
    require 'dbconn.php';

    $sql = "SELECT * FROM users WHERE sessionKey = '$sessionKey' AND userName = '$userName';";
    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows > 0) {
        return true;
    }
    else {
        return false;
    }
}

/* Verify admin has permission to take action. */
function isValidAdmin($sessionKey)
{
    require 'dbconn.php';

    $sql = "SELECT * FROM users WHERE sessionKey = '$sessionKey' AND isAdmin = 1;";
    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows > 0) {
        return true;
    }
    else {
        return false;
    }
}

/* Manage adding/updating records in archiveFolder table. */
function archiveFolder($folderId, $userId) {
    require 'dbconn.php';
    $now = date('Y-m-d H:i:s');

    $sql = "SELECT 1 FROM `archiveFolder` WHERE `userId` = $userId AND `folderId` = $folderId;";
    if (!$result = $con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows > 0) {
        $sql = "UPDATE archiveFolder SET `updated` = '$now' WHERE userId = $userId and folderId = $folderId";
    }
    else {
        $sql = "INSERT INTO `archiveFolder` (`userId`, `folderId`, `added`, `updated`, `archived`) VALUES ($userId, $folderId, '$now', '$now', 0);";
    }
    if (!$con->query($sql)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }
    return true;
}

} /* END UTIL CLASS */
?>