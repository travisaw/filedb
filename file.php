<?php

/* authcheck.php DOESN'T SEEM TO BE DOING ANYTHING HERE */
/* include authcheck.php; */

/* Check if authentication cookie is valid - if not return to login page */

//ini_set('upload_max_filesize', '20M');

require_once 'util.php';
require_once "dbconn.php";
$userAuth = new util;

$userDetails = $userAuth->validSession();
$browseMode = $userAuth->getParam("browse.mode", "1");
$userName = $userDetails[0];
$userId = $userDetails[1];
$Columns = $userDetails[2];
$userKey = $userDetails[3];
$multiUpload = $userAuth->getParam("file.multiUpload", 1);

$folderId = 0;
if(isset($_GET['folderid'])) { $folderId = $_GET['folderid']; }

?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<title>Upload File</title>
<link rel="stylesheet" type="text/css" href="assets/css/default.css">
<script src="assets/js/jquery-3.7.1.min.js"></script>
</head>
<body>
<form method="post" enctype="multipart/form-data" action="file.php?folderid=<?php echo $folderId; ?>">
<input type="hidden" name="inFolderId" value="1" />
<div>
<table class="headtable">
<tr>
    <td class="headtable">
        <input type="hidden" name="MAX_FILE_SIZE" value="21000000">
    </td>
    <td class="headtable">
        <input name="userfile[]" type="file" id="userfile" <?php if ($multiUpload == 1) { echo "multiple=''"; } ?>>
    </td>
    <td class="headtable">
        <input name="upload" type="submit" class="box" id="upload" value=" Upload ">
    </td>
</tr>
<tr>
    <td class="headtable"></td>
    <td class="headtable">
        <input type="text" name="foldername">
    </td>
    <td class="headtable">
        <input name="newfolder" type="submit" class="box" id="newfolder" value=" New Folder ">&nbsp;&nbsp;&nbsp;
    </td>
</tr>
<tr>
    <td class="headtable"></td>
    <td class="headtable"></td>
    <td class="headtable">
        <input name="arcfolder" type="button" class="box" id="arcfolder" value=" Archive Folder " onclick="archivefolder(<?php echo "$folderId, $userId, '$userKey'"; ?>)">&nbsp;&nbsp;<label id="lblArchiveStatus"></label>
    </td>
</tr>
</table>
</div>
</form>
<form method="post" action="file.php?folderid=<?php echo $folderId; ?>">
<div>
<br>
<table class="headtable">
    <tr>
        <td class="headtable">
            <input type="submit" name="refreshFile" class="InputButton" value="Refresh Files">&nbsp;&nbsp;
            <input type="submit" name="settings" class="InputButton" value="Settings"><br>
        </td>
        <td class="headtable">

        </td>
    <tr>
        <td class="headtable">
            <input type="text" name="filterText">
        </td>
        <td class="headtable">
            <input type="submit" name="filter" class="InputButton" value="Filter">
        </td>
    </tr>
</table>
<br>
</div>
</form>
<div>
<?php

    $rootFound = 0;
    $query5 = "SELECT name FROM folder WHERE id = $folderId; ";
    if (!$result = $con->query($query5)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    while ($row = $result->fetch_row())
    {
        $folderString = "<a href='file.php?folderid=$folderId'>$row[0]</a>";
    }

    $newChild = $folderId;
    if ($folderId == 0) { $rootFound = 1; }
    while ($rootFound == 0)
    {
        $query5 = "SELECT A.parentInstance, B.name FROM objectHierarchy A INNER JOIN folder B ON A.parentInstance = B.id ";
        $query5 .= "WHERE A.parentComponent = 2 AND A.childComponent = 2 AND A.childInstance = $newChild;";
        //echo $query5;
        if (!$result = $con->query($query5)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }
        while ($row = $result->fetch_row())
        {
            $folderString = "<a href='file.php?folderid=$row[0]'>$row[1]</a>&nbsp;&#8594;&nbsp" . $folderString;
            $newChild = $row[0];
            if ($row[0] == 0) { $rootFound = 1; }
        }
    }

    echo $folderString;
?>
</div>
<br>
<div>
<?php

    if(isset($_POST["settings"])) {
        header("Location: settings.php");
    }

    //SECTION 1 - Check if upload button clicked and save data.

    if(isset($_POST["upload"]))
    {
        $fileCount = 0;
        $errorCount = 0;
        for ($i = 0; $i < count($_FILES['userfile']['name']); $i++) {

            if($_FILES["userfile"]["size"][$i] > 0 && $_FILES["userfile"]["error"][$i] == 0) {
                $fileName = $_FILES["userfile"]["name"][$i];
                $tmpName  = $_FILES["userfile"]["tmp_name"][$i];
                $fileSize = $_FILES["userfile"]["size"][$i];
                $fileType = $_FILES["userfile"]["type"][$i];

                $fp      = fopen($tmpName, "r");
                $content = fread($fp, filesize($tmpName));
                $content = addslashes($content);
                fclose($fp);

                if(!get_magic_quotes_gpc()) {
                    $fileName = addslashes($fileName);
                }

                $query1 = "CALL `insertFile`('$userName', '$fileName', '$fileType', '$fileSize', '$content', '$folderId');";
                if (!$con->query($query1)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

                $fileCount++;
			}

            if ($_FILES["userfile"]["error"][$i] !== 0) {
                $errorCount++;
            }
		}

        if ($fileCount == 0) {
            echo "<b>Error uploading files</b><br><br>";
        }
        if ($fileCount == 1) {
            echo "<b>File $fileName uploaded with $errorCount errors</b><br><br>";
        }
        else {
            echo "<b>$fileCount files uploaded with $errorCount errors</b>\n";
        }
    }

    //SECTION 2 - Check if new folder button clicked and save folder.

    if(isset($_POST["newfolder"]))
    {
        if (empty($_POST['foldername']))
        {
            echo "Folder name cannot be blank";
        }
        else
        {
            $folderName = $_POST['foldername'];
            $query1 = "CALL `insertFolder`('$userName', '$folderName', '$folderId');";
            if (!$con->query($query1))
            {
                die ("CALL failed: (" . $con->errno . ") " . $con->error);
            }
            else
            {
                echo "<b>Created new folder $folderName</b><br><br>";
            }
        }
    }

    //SECTION 3 - Display List of Files

    $query2 = "SELECT F.`childComponent`, F.`id`, F.`name`, F.`parentInstance` FROM ";
    $query2 .= "(SELECT A.`parentInstance`, A.`childComponent`, B.`id`, B.`name` ";
    $query2 .= "FROM `objectHierarchy` AS A ";
    $query2 .= "INNER JOIN `folder` AS B ON A.`childComponent` = 2 AND A.`childInstance` = B.`id` ";
    $query2 .= "WHERE A.`userId` = $userId AND B.`userId` = $userId AND A.`parentComponent` = 2 AND A.`parentInstance` = $folderId ";
    $query2 .= "UNION ";
    $query2 .= "SELECT A.`parentInstance`, A.`childComponent`, B.`id`, B.`name` ";
    $query2 .= "FROM `objectHierarchy` AS A ";
    $query2 .= "INNER JOIN `file` AS B ON A.`childComponent` = 1 AND A.`childInstance` = B.`id` ";
    $query2 .= "WHERE A.`userId` = $userId AND B.`userId` = $userId AND A.`parentComponent` = 2 AND A.`parentInstance` = $folderId) AS `F` ";
    $query2 .= "INNER JOIN `component` AS `CP` ON CP.`id` = F.`childComponent` ";

    if(isset($_POST["filter"])) {
        $filterText = $_POST["filterText"];
        $query2 .= " WHERE F.name like '%$filterText%'";
    }
    $query2 .= " ORDER BY CP.sortOrder, F.name;";

    if (!$result = $con->query($query2)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }

    if ($result->num_rows == 0) {
        echo "Folder is empty<br>";
    }
    else
    {
        /* Draw Table */
        $Count = 1;
        if ($browseMode == 1)
        {
            $fileUrl = "fileget.php";
        }
        else
        {
            $fileUrl = "slideshow.php";
        }

        echo "<table class='contenttable'>\n";

        while ($row = $result->fetch_row()) {
            /* If Counter reset start new row */
            if ($Count == 1){
                echo "\t\t<tr>\n";
            }
            /* Build table cell */
            echo "\t\t\t<td class=\"contenttable\">";
            if ($row[0] == 1){ echo "<a href='$fileUrl?id=$row[1]&fid=$row[3]'>$row[2]</a>"; }
            elseif ($row[0] == 2){ echo "<a href='file.php?folderid=$row[1]'>/$row[2]</a>"; }
            echo "</td>\n";

            /* If exceed number of columns put table row end */
            if ($Count >= $Columns) {
                echo "\t\t</tr>\n";
                $Count = 0;
            }
            $Count++;
        }
        echo "\t</table>\n";
    }
    $con->close();

    ?>
<br><br>
</div>
</body>
<script>
function archivefolder(t,a,e){var l="action=archivefolder";l+="&folderid="+t,l+="&userid="+a,l+="&token="+e,$.ajax({type:"GET",url:"ajax.php",data:l,success:function(t){$("#lblArchiveStatus").text(t),"Submitted"==t?$("#lblArchiveStatus").addClass("txtUpdatePass"):$("#lblArchiveStatus").addClass("txtUpdateFail"),setTimeout(function(){$("#lblArchiveStatus").text("")},3e3)},error:function(t){$("#lblArchiveStatus").text("Call Failed"),$("#lblArchiveStatus").addClass("txtUpdateFail"),setTimeout(function(){$("#lblArchiveStatus").text("")},3e3),console.log(t)}}),$("#txtPassword").val(""),$("#txtPasswordConf").val("")}
</script>
</html>