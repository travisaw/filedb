
 *************************************************************************************
 *
 *   File Database
 *
 *   TRAVIS WILSON (travis@totaltravisty.com)
 *   March 29 2014
 *   v3.0
 *
 *   CONTENTS:
 *      1) INTRODUCTION
 *      2) FILE DESCRIPTION
 *      3) TABLE DESCRIPTION
 *      4) CONFIGURATION
 *
 *************************************************************************************

 1) INTRODUCTION
    File datadase is provided the ability for multiple users to upload and view files.
    Files are loaded into a database table which is sometimes more desireable than saving
    to a file system.  Users can then view the files in a web browser or download them if
    the browser doesn't support them.


 2) FILE DESCRIPTION:

    admin.php
        Page to control admin settings.  Called from settings.php if user is admin.
    ajax.php
        "Webservice" page called by AJAX functionality in settings.php.
    auth.php
        User Login Page - Sets authentication token.
    dataInstall.sql
        Script to install database schema.
    dbconn.php
        File containing SQL connection information.
    default.css
        Default Style Sheet.
    favicon.ico
        15x15 Bookmark Icon.
    file.php
        Page where user uploads files and lists users files.
    fileget.php
        Retrieves file of given file id.
    README.txt
        This file.
    settings.php
        Page for users to reset password and set number of files per display row.
    util.php
        Contains functions for operations within application.

 3) TABLE DESCRIPTION:

    authLog:
        Log of authentication attempts.
    authType:
        Reference tables linking authentication type ids to a description.
    file:
        Contains file owner, file name, file type, file size and file data in binary form.
    param:
        Contains system parameters
    user:
        Contains User's Name, Password, Session Id and Files Per Row.

 4) CONFIGURATION
    - Run dataInstall.sql on desired database.
    - Configure database connction settings in dbconn.php.
    - Move .php files to desired directory webserver root.
    - Configure file.php as default page in webserver folder.

    Additional Notes:
    Edit the entry 'sessionTimeout' in the param table to change session timeout.
    Default is 10 minutes (600 seconds).

    Edit php.ini to allow uploading of larger files.  The default is set to 20M in file.php
    upload_max_filesize = 20M
    post_max_size = 20M
