<?php

require_once 'util.php';
$userAuth = new util;

$userDetails = $userAuth->validSession();
$userName = $userDetails[0];
$userId = $userDetails[1];

/* HEADER TAGS TO SET PAGE EXPIRATION */
header("Expires: " . time() - 1);
header("Cache-Control: no-cache");
header("Pragma: no-cache");

if(isset($_GET['id']))
    {
    // if id is set then get the file with the id from database
    $id = $_GET['id'];

    // verify user has permissions to view file first
    if ($userAuth->userPermission($userId, $id) == FALSE) { die("<h1>Unknown File</h1>"); }

    //include 'library/config.php';
    require 'dbconn.php';

    if ($userAuth->getParam('authLog.enabled', '0') > 1) { $userAuth->authLog(4, $userName, $_GET['id']); }

    $id    = $_GET['id'];
    $query = "SELECT name, type, size, content " .
             "FROM file WHERE id = '$id'";

    if (!$result = $con->query($query)) { die ("CALL failed: (" . $con->errno . ") " . $con->error); }
    if ($result->num_rows > 0) {
        $row = $result->fetch_row();
    }
    $name = $row[0];
    $type = $row[1];
    $size = $row[2];
    $content = $row[3];

    header("Content-length: $size");
    header("Content-type: $type");
    //USING DISPOSITION: attachment WILL FORCE DOWNLOAD
    //header("Content-Disposition: attachment; filename=$name");
    //USING DISPOSITION: inline WILL DISPLAY CONTENT IN BROWSER IF BROWSER KNOWS WHAT TO DO WITH IT
    if ($userAuth->getParam('fileget.disposition', 'inline') == 'inline') {
        header('Content-Disposition: inline');
    }
    else {
        header('Content-Disposition: attachment');
    }

    //echo $content;
    print $content;

    $con->close();
    exit;
    }

?>
