<?php

// Extract URLs from webpage and download

require_once 'download_functions.php';
require_once 'download_pp_functions.php';

$startTime = time();
$inPrintUrl = 1;
$inExecuteDownload = 1;
$inDestDir = "./dl/";
$inDestPrefix = "px";   // Destination File Prefix
$inDestFilePad = 4;     // Destination file number pad
$inDestStart = 1;    // Destination file start number
$inDestIncrement = 1;   // Destination file increment
$inDestSuffix = ".jpg"; //Destination file suffix

$sourceFile = "./url.txt";
$setIncrement = 25;
$setCount = 1;

// Create directory using destination + prefix
$moveDir = $inDestDir . $inDestPrefix . '/';
if (!file_exists($moveDir)) {
    mkdir($moveDir, 0777, true);
}

$handle = fopen($sourceFile, "r");
if ($handle) {
    while (($line = fgets($handle)) !== false)
    {
        // process the line read.
        $inSourceUrl = preg_replace( "/\r|\n/", "", $line );

        echo "Start Download of Set $setCount" . PHP_EOL;
        $currentIncrement = downloadPage(
            $inPrintUrl,        // print Url
            $inExecuteDownload, // execute Download
            $inSourceUrl,       // source Url
            $moveDir,           // dest Dir
            $inDestPrefix,      // dest Prefix
            $inDestFilePad,     // dest FilePad
            $inDestStart,       // dest Start
            $inDestIncrement,   // dest Increment
            $inDestSuffix       // dest Suffix
        );
        echo "Finish Download of Set $setCount" . PHP_EOL;
        $setCount++;

        if ($setIncrement > 0)
        {
            $inDestStart = $inDestStart + $setIncrement;
        }
        else
        {
            $inDestStart = $currentIncrement;
        }

    }

    fclose($handle);

}
else
{
    echo "Error opening URL source file " . $sourceFile . PHP_EOL;
}

$finishTime = time();
echo "Started at : " . date("F j, Y, g:i A T", $startTime) . PHP_EOL;
echo "Finished at: " . date("F j, Y, g:i A T", $finishTime) . PHP_EOL;

?>