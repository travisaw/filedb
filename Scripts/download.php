<?php

// Script to download files in bulk using url source pattern and destination file pattern

$printUrl = 1;
$executeDownload = 1;

$sourceUrl = "<URL>";
$sourceFilePrefix = ""; // Filename prefix
$sourceFilePad = 2; // NUMBER OF LEADING 0s
$sourceFileStart = 1; // Value to Start At
$sourceFileIncrement = 1; // Value to Increment
$sourceFileSuffix = ".jpg"; // File extension
$sourceCount = 18;

$destDir = "/home/travis/dl/";
$destPrefix = ""; // Destination File Prefix
$destFilePad = 2; // Destination file number pad
$destStart = 1; // Destination file start number
$destIncrement = 1; // Destination file increment
$destSuffix = ".jpg"; //Destination file suffix

$currentCount = 1;

while ($currentCount <= $sourceCount)
{
    // Parse source url and destination file strings
    $sourceFile = $sourceFilePrefix . str_pad($sourceFileStart, $sourceFilePad, "0", STR_PAD_LEFT) . $sourceFileSuffix;
    $url = $sourceUrl . $sourceFile;
    $destFile = $destPrefix . str_pad($destStart, $destFilePad, "0", STR_PAD_LEFT) . $destSuffix;

    // Download files
    if ($executeDownload > 0)
    {
        if (download_file($url, $destDir, $destFile, $printUrl))
        {
            $destStart += $destIncrement;
        }
    }

    // Increment counters
    $sourceFileStart += $sourceFileIncrement;
    $currentCount++;

}

?>