<?php

require "dbconn.php";
require "upload_functions.php";

// Load Configuration
$configJson = file_get_contents("upload.json");
$config = json_decode($configJson, true);

// Set local variables
$sourceDir = $config['SourceDir'];
$userName = $config['UserName'];
$overwriteExisting = $config['OverwriteExisting'];
$totalFiles = 0;

// Check source directory exists
if (!is_dir($sourceDir)) { die("Source directory does not exist! Exiting." . PHP_EOL); }

// Get list of subdirectories
$dirs = glob($sourceDir . '*', GLOB_ONLYDIR);

$startTime = time();

// Call upload routine
foreach ($dirs as $dir) {
    $destFolder = str_replace($sourceDir, '', $dir);
    $dir .= '/';
    $fileCount = upload($dir, $destFolder, $userName, $overwriteExisting, $con);
    $totalFiles = $totalFiles + $fileCount;
}

// Summarize
$finishTime = time();
$totalTime = $finishTime - $startTime;
echo "Started at : " . date("F j, Y, g:i A T", $startTime) . PHP_EOL;
echo "Finished at: " . date("F j, Y, g:i A T", $finishTime) . PHP_EOL;
echo "Processed a total of $totalFiles files" . PHP_EOL;
if ($totalTime != 0) {
    echo "At a rate of " . round($totalFiles / $totalTime, 4) . " files per second" . PHP_EOL;
}
else {
    echo "At a rate of $totalFiles files per second" . PHP_EOL;
}

?>
