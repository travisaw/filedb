<?php

require "../dbconn.php";

$startTime = time();

/* Get File/Folder  count */
$sql = "UPDATE `archiveFolder` A
LEFT JOIN (SELECT `parentComponent`, `parentInstance`, COUNT(1) AS `recCount` FROM `objectHierarchy` GROUP BY `parentComponent`, `parentInstance`) B 
ON A.folderId = B.parentInstance AND B.parentComponent = 2
SET fileCount = IFNULL(B.`recCount`, 0)
WHERE A.`archived` = 0;";

$con->query($sql);

/* Get FileSize */
$sql = "UPDATE `archiveFolder` AF
INNER JOIN ( 
SELECT OH.`parentInstance` AS `folderId`, SUM(F.`size`) AS `fileSize`
FROM `objectHierarchy` OH
INNER JOIN `file` F
ON OH.`childInstance` = F.`id` AND OH.`childComponent` = 1
WHERE OH.`parentComponent` = 2
GROUP BY OH.`parentComponent`, OH.`parentInstance`) SUB
ON AF.`folderId` = SUB.`folderId`
SET AF.`fileSize` = SUB.`fileSize`
WHERE AF.`archived` = 0;";

$con->query($sql);

$finishTime = time();
echo "Started at : " . date("F j, Y, g:i A T", $startTime) . PHP_EOL;
echo "Finished at: " . date("F j, Y, g:i A T", $finishTime) . PHP_EOL;


?>