<?php

// Script to upload files in a given directory to the database. Provided folder name and user name.

require "../dbconn.php";
$sortMultiplier = 2;

// ***********************************************************************************************************************
// CREATE SORT TABLE
$sql1 = "
CREATE TABLE IF NOT EXISTS `sort` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `fileId` INT(10) UNSIGNED NOT NULL,
    `userId` INT(10) UNSIGNED NOT NULL,
    `fileName` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `IX_sort_fileId` (`fileId`) USING BTREE,
    UNIQUE INDEX `IX_sort_user_file` (`userId`, `fileName`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;";

$query1 = $con->prepare($sql1);
if (!$query1->execute())
{
    die("CALL failed when createing sort table: (" . $con->errno . ") " . $con->error) . PHP_EOL;
}

// ***********************************************************************************************************************
// CLEAR SORT TABLE IN THE CASE IT ALREADY EXISTS
$sql2 = "TRUNCATE TABLE `sort` ;";
$query2 = $con->prepare($sql2);
if (!$query2->execute())
{
    die("CALL failed when truncating sort table: (" . $con->errno . ") " . $con->error) . PHP_EOL;
}

// ***********************************************************************************************************************
// POPULATE SORT TABLE
$sql3 = "INSERT INTO `sort` (`fileId`, `userId`, `fileName`) SELECT `id`, `userId`, `name` FROM `file` ORDER BY `userId`, `name`;";
$query3 = $con->prepare($sql3);
if (!$query3->execute())
{
    die("CALL failed when populating sort table: (" . $con->errno . ") " . $con->error) . PHP_EOL;
}

// ***********************************************************************************************************************
// UPDATE OBJECT HIERARCHY
$sql4 = "UPDATE `objectHierarchy` A INNER JOIN `sort` B ON A.`userId` = B.`userId` AND A.`childComponent` = 1 AND A.`childInstance` = B.`fileId` SET `sort` = B.`id` * $sortMultiplier;";
$query4 = $con->prepare($sql4);
if (!$query4->execute())
{
    die("CALL failed when updating object hierarchy: (" . $con->errno . ") " . $con->error) . PHP_EOL;
}

// ***********************************************************************************************************************
// DROP SORT TABLE
$sql5 = "DROP TABLE `sort`;";
$query5 = $con->prepare($sql5);
if (!$query5->execute())
{
    die("CALL failed when dropping sort table: (" . $con->errno . ") " . $con->error) . PHP_EOL;
}


?>
