<?php

// Download files from a list of URLs in a source file.

require_once 'download_functions.php';

$printUrl = 1;
$executeDownload = 1;

$sourceFile = "/home/travis/scripts/filedb/url.txt";

$destDir = "/home/travis/dl/";
$destPrefix = "mn"; // Destination File Prefix
$destFilePad = 3; // Destination file number pad
$destStart = 240; // Destination file start number
$destIncrement = 1; // Destination file increment
$destSuffix = ".jpg"; //Destination file suffix

$currentCount = 1;

$handle = fopen($sourceFile, "r");
if ($handle) {
    while (($line = fgets($handle)) !== false) {
        // process the line read.
        $url = preg_replace( "/\r|\n/", "", $line );

        $destFile = $destPrefix . str_pad($destStart, $destFilePad, "0", STR_PAD_LEFT) . $destSuffix;

        // Download files
        if ($executeDownload > 0)
        {
            if (download_file($url, $destDir, $destFile, $printUrl))
            {
                $destStart += $destIncrement;
            }
        }

        // Increment counters
        $currentCount++;

    }

    fclose($handle);
}
else
{
    echo "Error opening URL source file " . $sourceFile . PHP_EOL;
}

?>