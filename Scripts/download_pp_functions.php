<?php

// Extract URLs from webpage and download

require_once 'download_functions.php';

function downloadPage(
    $printUrl,
    $executeDownload,
    $sourceUrl,
    $destDir,
    $destPrefix, // Destination File Prefix
    $destFilePad, // Destination file number pad
    $destStart, // Destination file start number
    $destIncrement, // Destination file increment
    $destSuffix //Destination file suffix
)

{

$currentCount = 1;

//////////////////////////////////////////////////////////////////////////////////

$html = file_get_contents($sourceUrl);

libxml_use_internal_errors(true);
$dom = new DOMDocument();
$dom->loadHTML($html);
$xpath = new DOMXPath($dom);

$html = '';

// Get DIV
foreach ($xpath->query('//div[@id="main"]/node()') as $node)
{
    $html .= $dom->saveHTML($node);
}

// REMOVE ul TAGS
$html = strstr($html, '<ul class="wookmark-initialised" id="tiles">');
$html = str_replace('<ul class="wookmark-initialised" id="tiles">', '', $html);
$html = str_replace('</ul>', '', $html);
$html = trim($html);
$html = preg_replace( "/\r|\n/", "", $html );

// CONVERT li ITEMS TO ARRAY
$myListArray = explode("</li>", $html);

// LOOP THROUGH ARRAY TO PULL OUT LINKS
$fileArray = array();

foreach ($myListArray as $listItem)
{
    if (!empty($listItem) && substr(trim($listItem), 0, 3) == "<li")
    {
        $listItem = str_replace('<li class="thumbwook"><a class="rel-link" href="', '', $listItem);
        $listItem = substr($listItem, 0, strpos($listItem, '"'));
        array_push($fileArray, $listItem);
    }
}

////////////////////////////////////////////////////////////////////////////////

foreach ($fileArray as $url)
{
    // process the line read.
    $url = preg_replace( "/\r|\n/", "", $url );

    $destFile = $destPrefix . str_pad($destStart, $destFilePad, "0", STR_PAD_LEFT) . $destSuffix;

    // Download files
    if ($executeDownload > 0)
    {
        if (download_file($url, $destDir, $destFile, $printUrl))
        {
            $destStart += $destIncrement;
        }
    }

    // Increment counters
    $currentCount++;
}

return $destStart;

} // end FUNCTION


?>