<?php

// CONTAINS FUNCTIONS USED BY download.php AND downloadl_list.php

function download_file($url, $destDir, $destFile, $printUrl)
{
    $dest = $destDir . $destFile;

    // Print URL
    if ($printUrl > 0)
    {
        echo $url . "   " . $dest . PHP_EOL;
    }

    // Download File
    if(get_http_response_code($url) != "200")
    {
        echo "  ERROR: URL Not Valid!" . PHP_EOL;
    }
    else
    {
        if (is_writable($destDir))
        {
            file_put_contents($dest, fopen($url, 'r'));
            echo "  Success" . PHP_EOL;
            return true;
        }
        else
        {
            echo "  ERROR: Destination File Unreachable!" . PHP_EOL;
            return false;
        }
    }
}

function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}

?>