<?php

require "dbconn.php";
require "upload_functions.php";

// Load Configuration
$configJson = file_get_contents("upload.json");
$config = json_decode($configJson, true);

// Set local variables
$sourceDir = $config['SourceDir'];
$folderName = $config['DestinationFolder'];
$userName = $config['UserName'];
$overwriteExisting = $config['OverwriteExisting'];

// Check source directory exists
if (!is_dir($sourceDir)) { die("Source directory does not exist! Exiting." . PHP_EOL); }

$startTime = time();

// Call upload routine
$fileCount = upload($sourceDir, $folderName, $userName, $overwriteExisting, $con);

// Summarize
$finishTime = time();
$totalTime = $finishTime - $startTime;
echo "Started at : " . date("F j, Y, g:i A T", $startTime) . PHP_EOL;
echo "Finished at: " . date("F j, Y, g:i A T", $finishTime) . PHP_EOL;
if ($totalTime != 0) {
    echo "At a rate of " . round($fileCount / $totalTime, 4) . " files per second" . PHP_EOL;
}
else {
    echo "At a rate of $fileCount files per second" . PHP_EOL;
}

?>
