<?php

require "../dbconn.php";

$startTime = time();

echo "Clean Up objectHierarchy..." . PHP_EOL;
execute($con, "DELETE OH FROM `objectHierarchy` OH LEFT JOIN `folder` F ON OH.`parentInstance` = F.`id` AND OH.`parentComponent` = 2 WHERE F.`id` IS NULL;");
execute($con, "DELETE OH FROM `objectHierarchy` OH LEFT JOIN `folder` F ON OH.childInstance = F.id WHERE F.id IS NULL AND OH.childComponent = 2;");

echo "Analyze Tables..." . PHP_EOL;
execute($con, "ANALYZE TABLE `authLog`;");
execute($con, "ANALYZE TABLE `file`;");
execute($con, "ANALYZE TABLE `objectHierarchy`;");

echo "Optimize Tables..." . PHP_EOL;
execute($con, "OPTIMIZE TABLE `authLog`;");
execute($con, "OPTIMIZE TABLE `file`;");
execute($con, "OPTIMIZE TABLE `objectHierarchy`;");

$finishTime = time();
echo "Started at : " . date("F j, Y, g:i A T", $startTime) . PHP_EOL;
echo "Finished at: " . date("F j, Y, g:i A T", $finishTime) . PHP_EOL;

function execute($con, $sql) {
    $query = $con->prepare($sql);
    $query->execute();
}

?>
