<?php

// Extract URLs from webpage and download

require_once 'download_functions.php';
require_once 'download_pp_functions.php';

$printUrl = 1;
$executeDownload = 1;

$sourceUrl = "";

$destDir = "/home/travis/dl/";
$destPrefix = "aa"; // Destination File Prefix
$destFilePad = 3; // Destination file number pad
$destStart = 1; // Destination file start number
$destIncrement = 1; // Destination file increment
$destSuffix = ".jpg"; //Destination file suffix

$currentCount = 1;

$currentIncrement = downloadPage(
            $printUrl,        // print Url
            $executeDownload, // execute Download
            $sourceUrl,       // source Url
            $destDir,         // dest Dir
            $destPrefix,      // dest Prefix
            $destFilePad,     // dest FilePad
            $destStart,       // dest Start
            $destIncrement,   // dest Increment
            $destSuffix       // dest Suffix
        );
