<?php

function upload($sourceDir, $folderName, $userName, $overwriteExisting, $con) {
    $files = scandir($sourceDir, 0);

    $fileCount = 0;
    $errorCount = 0;
    
    // ***********************************************************************************************************************
    // GET USER ID
    $query2 = $con->prepare("SELECT `id` FROM `users` WHERE userName = '$userName';");
    if (!$query2->execute())
    {
        die("CALL failed when looking up user: (" . $con->errno . ") " . $con->error) . PHP_EOL;
    }

    $result2 = $query2->get_result();
    $row2 = $result2->fetch_assoc();

    if (isset($row2['id']))
    {
        $userId = $row2['id'];
        echo "User Id is " . $row2['id'] . PHP_EOL;
    }
    else
    {
        die("User ID not found with name '$userName'" . PHP_EOL);
    }

    // ***********************************************************************************************************************
    // GET FOLDER ID
    $query5 = $con->prepare("SELECT `id` FROM `folder` WHERE `name` = '$folderName';");
    if (!$query5->execute())
    {
        die("CALL failed when looking up user: (" . $con->errno . ") " . $con->error) . PHP_EOL;
    }

    $result5 = $query5->get_result();
    $row5 = $result5->fetch_assoc();

    if (isset($row5['id']))
    {
        $folderId = $row5['id'];
        echo "Folder Id is " . $row5['id'] . PHP_EOL;
    }
    else
    {
        die("Folder ID not found with name '$folderName'" . PHP_EOL);
    }

    // ***********************************************************************************************************************
    // LOOP THROUGH SOURCE DIRECTORY AND UPLOAD
    for ($i = 0; $i < count($files); $i++)
    {
        if ($files[$i] !== '.' && $files[$i] !== '..')
        {
            $fullFileName = $sourceDir . $files[$i];
            $fileName = $files[$i];
            $fileSize = filesize($fullFileName);
            //$fileType = 'image/jpeg'; // THIS IS FETCHED FROM THE DATABASE
            $typeFound = 0;
            $uploadFile = 0;
            //echo $fileName . "  " . $fileSize . PHP_EOL;

            // GET FILE TYPE
            $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);
            $fileExt = '.' . $fileExt;

            $query6 = $con->prepare("SELECT `mimeType` FROM `fileType` WHERE `extension` = '$fileExt';");
            if (!$query6->execute())
            {
                die("CALL failed when looking up existing file: (" . $con->errno . ") " . $con->error) . PHP_EOL;
            }

            $result6 = $query6->get_result();
            $row6 = $result6->fetch_assoc();

            if (isset($row6['mimeType']))
            {
                $fileType = $row6['mimeType'];
                $typeFound = 1;
            }
            else
            {
                echo "MIME type not found for extension '$fileExt'. Skipping $fileName" . PHP_EOL;
            }

            // CHECK IF FILE ALREADY EXISTS
            $query3 = $con->prepare("SELECT `id` FROM `file` WHERE `UserId` = $userId AND `Name` = '$fileName';");
            if (!$query3->execute())
            {
                die("CALL failed when looking up existing file: (" . $con->errno . ") " . $con->error) . PHP_EOL;
            }

            $result3 = $query3->get_result();
            $row3 = $result3->fetch_assoc();

            if (isset($row3['id']))
            {
                $fileId = $row3['id'];
                echo "File aready exits... ";
                if ($overwriteExisting == 1)
                {
                    echo "deleting" . PHP_EOL;
                    $query4 = $con->prepare("DELETE FROM `file` WHERE `id` = $fileId;");
                    if (!$query4->execute())
                    {
                        die("CALL failed when looking up existing file: (" . $con->errno . ") " . $con->error) . PHP_EOL;
                    }
                    $uploadFile = 1;
                }
                else
                {
                    echo "skipping" . PHP_EOL;
                }
            }
            // FILE DOESN'T EXIST. SAFE TO ATTEMPT UPLOAD
            else
            {
                $uploadFile = 1;
            }

            // IF SAFE TO UPLOAD THEN DO SO
            if ($uploadFile == 1 && $typeFound == 1)
            {
                $fp      = fopen($fullFileName, "r");
                $content = fread($fp, $fileSize);
                $content = addslashes($content);
                fclose($fp);

                $query1 = "CALL `insertFile`('t', '$fileName', '$fileType', '$fileSize', '$content', $folderId);";
                if (!$con->query($query1))
                {
                    echo ("CALL failed: (" . $con->errno . ") " . $con->error) . PHP_EOL;
                    $errorCount++;
                }
                else
                {
                    echo "Uploaded " . $fileName . "  " . $fileSize . PHP_EOL;
                    $fileCount++;
                }
            }
        }
    }

    echo "Uploaded $fileCount files with $errorCount errors" . PHP_EOL . PHP_EOL;
    return $fileCount;

}

?>
