<?php

require "../dbconn.php";

$startTime = time();

/* Get File/Folder  count */
$sql = "SELECT `folderId`, `fileCount`, `fileSize` FROM `archiveFolder` WHERE `archived` = 0;";

$result = $con->query($sql);

$folderCount = 0;
$fileCount = 0;
$fileSize = 0;


while($row = $result->fetch_row())
{
    $folderCount++;
    $fileCount += $row[1];
    $fileSize += $row[2];
}

echo "Archive Details:" . PHP_EOL;
echo "Folders:   $folderCount" . PHP_EOL;
echo "Files:     $fileCount" . PHP_EOL;
echo "File Size: " . round($fileSize/1000000, 2) . " MB" . PHP_EOL;

$run = readline('Do you want to archive this data?: ');

if ($run == 'y')
{
    echo "Running..." . PHP_EOL;

    $sql = "DELETE F FROM `archiveFolder` AF
    INNER JOIN `objectHierarchy` OH
    ON AF.`folderId` = OH.`parentInstance` AND OH.`parentComponent` = 2
    INNER JOIN `file` F
    ON OH.`childInstance` = F.`id` AND OH.`childComponent` = 1
    WHERE AF.`archived` = 0;";

    $con->query($sql);

    $sql = "DELETE F FROM `archiveFolder` AF
    INNER JOIN `folder` F
    ON AF.`folderId` = F.`id`
    WHERE AF.`archived` = 0;";

    $con->query($sql);

    $now = date('Y-m-d H:i:s');
    $sql = "UPDATE archiveFolder SET `archived` = 1, `archivedDate` = '$now' WHERE `archived` = 0;";

    $con->query($sql);

    echo "Completed! Run DB Maintenence Script." . PHP_EOL;
}

$finishTime = time();
echo PHP_EOL;
echo "Started at : " . date("F j, Y, g:i A T", $startTime) . PHP_EOL;
echo "Finished at: " . date("F j, Y, g:i A T", $finishTime) . PHP_EOL;

?>