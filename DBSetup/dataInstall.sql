
/*---------------------------------------------------------------------------------------------*/
/* TABLE: authLog */
CREATE TABLE `archiveFolder` (
    `userId` INT(10) UNSIGNED NOT NULL,
    `folderId` INT(10) UNSIGNED NOT NULL,
    `added` DATETIME NULL DEFAULT NULL,
    `updated` DATETIME NULL DEFAULT NULL,
    `archived` TINYINT(3) UNSIGNED NULL DEFAULT NULL,
    `archivedDate` DATETIME NULL DEFAULT NULL,
    `fileCount` INT(10) UNSIGNED NULL DEFAULT NULL,
    `fileSize` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
    PRIMARY KEY (`userId`, `folderId`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

/*---------------------------------------------------------------------------------------------*/
/* TABLE: authLog */
CREATE TABLE IF NOT EXISTS `authLog` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `sourceIp` varchar(45) NOT NULL DEFAULT '',
    `userName` varchar(64) NOT NULL DEFAULT '',
    `authType` tinyint(4) NOT NULL DEFAULT '0',
    `fileId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `logTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------------------------------------*/
/* TABLE: authType */
CREATE TABLE IF NOT EXISTS `authType` (
    `id` tinyint(11) unsigned NOT NULL,
    `authDesc` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------------------------------------*/
/* TABLE: component */
CREATE TABLE `component` (
    `id` INT(11) UNSIGNED NOT NULL,
    `name` VARCHAR(50) NOT NULL,
    `sourceTable` VARCHAR(50) NOT NULL,
    `sortOrder` INT(11) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`)
)
COMMENT='System Components'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
DEFAULT CHARSET=utf8;

/*---------------------------------------------------------------------------------------------*/
/* TABLE: file */
CREATE TABLE IF NOT EXISTS `file` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `name` VARCHAR(255) NULL DEFAULT NULL,
    `type` VARCHAR(255) NULL DEFAULT NULL,
    `size` VARCHAR(20) NULL DEFAULT NULL,
    `content` MEDIUMBLOB NOT NULL,
    `dateAdded` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `IX_FileName` (`userId`, `name`)
)
COMMENT='Content of files uploaded'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

/*---------------------------------------------------------------------------------------------*/
/* TABLE: fileType */
CREATE TABLE IF NOT EXISTS `fileType` (
  `mimeType` varchar(100) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `subtype` varchar(100) DEFAULT NULL,
  `extension` varchar(100) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mimeType`)
)
COMMENT='File MIME types and extensions'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

/*---------------------------------------------------------------------------------------------*/
/* TABLE: folder */
CREATE TABLE IF NOT EXISTS `folder` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `name` VARCHAR(255) NULL DEFAULT NULL,
    `dateAdded` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)
COMMENT='Folders'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=0;

/*---------------------------------------------------------------------------------------------*/
/* TABLE: objectHierarchy */
CREATE TABLE `objectHierarchy` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userId` INT(11) UNSIGNED NOT NULL,
    `parentComponent` INT(11) UNSIGNED NOT NULL,
    `parentInstance` INT(11) UNSIGNED NOT NULL,
    `childComponent` INT(11) UNSIGNED NOT NULL,
    `childInstance` INT(11) UNSIGNED NOT NULL,
    `dateAdded` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `sort` INT(11) UNSIGNED NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `IX_objHierarchy` (`userId`, `parentComponent`, `parentInstance`, `childComponent`, `childInstance`)
)
COLLATE='utf8_general_ci'
COMMENT='Object objectHierarchy'
ENGINE=InnoDB
AUTO_INCREMENT=1;

/*---------------------------------------------------------------------------------------------*/
/* TABLE: param */
CREATE TABLE `param` (
    `valueKey` VARCHAR(32) NOT NULL,
    `valueString` VARCHAR(512) NULL,
    PRIMARY KEY (`valueKey`)
)
COMMENT='System Parameters'
COLLATE='utf8_general_ci'
ENGINE=InnoDB;


/*---------------------------------------------------------------------------------------------*/
/* TABLE: users */
CREATE TABLE `users` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `userName` VARCHAR(64) NOT NULL,
    `password` VARCHAR(512) NOT NULL,
    `sessionKey` VARCHAR(512) NULL,
    `sessionStamp` DATETIME NULL DEFAULT NULL,
    `isAdmin` TINYINT UNSIGNED NOT NULL DEFAULT '0',
    `viewColumns` TINYINT UNSIGNED NOT NULL DEFAULT '8',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `IX_UserName` (`userName`)
)
COMMENT='Users and user settings'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

/*---------------------------------------------------------------------------------------------*/
/* VIEW: viewPhoto */
CREATE OR REPLACE VIEW `fileDetails`
AS
SELECT format(count(`file`.`id`),0) AS `FileCount`,
       format(sum(`file`.`size`),0) AS `SumOfSize`
FROM `file`;

/*---------------------------------------------------------------------------------------------*/
/* VIEW: viewPhoto */
CREATE OR REPLACE VIEW `recentFolders`
AS
SELECT B.`name`, B.`dateAdded`
FROM `objectHierarchy` A
INNER JOIN `folder` B
ON A.childInstance = B.id
WHERE A.`childComponent` = 2
AND A.parentComponent = 2
AND A.parentInstance = 0
ORDER BY B.dateAdded DESC
LIMIT 50;

/*---------------------------------------------------------------------------------------------*/
/* VIEW: rptArchiveFolder */
CREATE VIEW rptArchiveFolder
AS
SELECT B.`userName`, C.`name`, A.`added`, A.`updated`
FROM `archiveFolder` A
INNER JOIN `users` B ON A.`userId` = B.`id`
INNER JOIN `folder` C ON A.`folderId` = C.`id`;

/*---------------------------------------------------------------------------------------------*/
/* INSERT Folder */
INSERT INTO `folder` (`userId`, `name`, `dateAdded`) VALUES (0, 'root', UTC_TIMESTAMP());
UPDATE `folder` SET id = 0 WHERE name = 'root';

/*---------------------------------------------------------------------------------------------*/
/* INSERT component */
INSERT INTO `component` (`id`, `name`, `sourceTable`, `sortOrder`) VALUES (1, 'File', 'file', 10);
INSERT INTO `component` (`id`, `name`, `sourceTable`, `sortOrder`) VALUES (2, 'Folder', 'folder', 5);

/*---------------------------------------------------------------------------------------------*/
/* INSERTS params*/
INSERT INTO param (`valueKey`, `valueString`) VALUES ('authLog.enabled', '1');
INSERT INTO param (`valueKey`, `valueString`) VALUES ('browse.mode', '1');
INSERT INTO param (`valueKey`, `valueString`) VALUES ('file.multiUpload', '1');
INSERT INTO param (`valueKey`, `valueString`) VALUES ('fileget.disposition', 'inline');
INSERT INTO param (`valueKey`, `valueString`) VALUES ('sessionTimeout', '600');
INSERT INTO param (`valueKey`, `valueString`) VALUES ('slideshow.paging', '1');
INSERT INTO param (`valueKey`, `valueString`) VALUES ('slideshow.pagesize', '20');
INSERT INTO param (`valueKey`, `valueString`) VALUES ('slideshow.piclink', '1');

/*---------------------------------------------------------------------------------------------*/
/* INSERT authType */
INSERT INTO `authType` (`id`, `authDesc`) VALUES (1, 'Failed Username');
INSERT INTO `authType` (`id`, `authDesc`) VALUES (2, 'Failed Password');
INSERT INTO `authType` (`id`, `authDesc`) VALUES (3, 'Successful Login');
INSERT INTO `authType` (`id`, `authDesc`) VALUES (4, 'File Read');

/*---------------------------------------------------------------------------------------------*/
/* INSERT Default ADMIN user */
INSERT INTO `users` (`userName`, `password`, `isAdmin`) VALUES ('admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1);

/*---------------------------------------------------------------------------------------------*/
/* PROCEDURE: InsertFile*/
/* Procedure updateRecipeIngredient - used to get ingredient id - if id not found create ingredient */
DELIMITER $$$
CREATE PROCEDURE `insertFile`
    (IN `inUserName` VARCHAR(64),
     IN `inFileName` VARCHAR(255),
     IN `inFileType` VARCHAR(255),
     IN `inFileSize` VARCHAR(20),
     IN `inFileContent` MEDIUMBLOB,
     IN `inFolderId` INT UNSIGNED)
    LANGUAGE SQL
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT 'Inserts file into file table'
BEGIN
    DECLARE varUserId INT UNSIGNED;
    DECLARE varFileId INT UNSIGNED;
    SELECT `id` INTO varUserId FROM users WHERE `userName` = inUserName;

    INSERT INTO `file` (`userId`, `name`, `type`, `size`, `content`)
    VALUES (varUserId, inFileName, inFileType, inFileSize, inFileContent);
    SELECT LAST_INSERT_ID() INTO varFileId;

    INSERT INTO `objectHierarchy` (`userId`,`parentComponent`,`parentInstance`,`childComponent`,`childInstance`)
    VALUES (varUserId, 2, inFolderId, 1, varFileId);
END
$$$

/*---------------------------------------------------------------------------------------------*/
/* Procedure insertFolder */
CREATE PROCEDURE `insertFolder`
    (IN `inUserName` VARCHAR(64),
     IN `inFolderName` VARCHAR(255),
     IN `inFolderId` INT UNSIGNED)
    LANGUAGE SQL
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY DEFINER
    COMMENT 'Inserts folder into folder table'
BEGIN
    DECLARE varUserId INT UNSIGNED;
    DECLARE varFolderId INT UNSIGNED;
    SELECT `id` INTO varUserId FROM users WHERE `userName` = inUserName;

    INSERT INTO `folder` (`userId`, `name`)
    VALUES (varUserId, inFolderName);
    SELECT LAST_INSERT_ID() INTO varFolderId;

    INSERT INTO `objectHierarchy` (`userId`,`parentComponent`,`parentInstance`,`childComponent`,`childInstance`)
    VALUES (varUserId, 2, inFolderId, 2, varFolderId);
END
$$$

/*---------------------------------------------------------------------------------------------*/
/* Procedure insertAuth */
CREATE PROCEDURE `insertAuth`
   (IN `inIp` VARCHAR(45),
    IN `inUser` VARCHAR(65),
	IN `inType` TINYINT,
    IN `inFileId` INT UNSIGNED
	)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Insert authentication result'
BEGIN
    DECLARE varAuth TINYINT;
    SELECT IFNULL(`valueString`, 0) INTO varAuth FROM param WHERE `valueKey` = 'authLog.enabled';

    IF varAuth <> 0 THEN
        INSERT INTO authLog (`sourceIp`, `userName`, `authType`, `fileId`) VALUES (inIp, inUser, inType, inFileId);
    END IF;

END
$$$

/*---------------------------------------------------------------------------------------------*/
/* Procedure insertUser */
CREATE PROCEDURE `insertUser`
   (IN `inUser` VARCHAR(64),
	IN `inPw` VARCHAR(512)
	)
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT 'Insert or Update User'
BEGIN
    DECLARE varUser TINYINT;
    SELECT IFNULL(COUNT(*), 0) INTO varUser FROM `users` WHERE `userName` = inUser;

    IF varUser = 0 THEN
        INSERT INTO `users` (`userName`, `password`) VALUES (inUser, inPw);
    ELSE
        UPDATE `users` SET password = inPw WHERE `userName` = inUser;
    END IF;

END
$$$

/*---------------------------------------------------------------------------------------------*/
/* Procedure fileType */

INSERT INTO `fileType` (`mimeType`, `type`, `subtype`, `extension`, `label`) VALUES
    ('*/*', '*', '*', '', 'Unknown'),
    ('application/acad', 'application', 'acad', '.dwg', 'AutoCAD drawing files'),
    ('application/andrew-inset', 'application', 'andrew-inset', '.ez', 'Andrew data stream'),
    ('application/clariscad', 'application', 'clariscad', '.ccad', 'ClarisCAD files'),
    ('text/csv', 'text', 'csv', '.csv', 'Text - Comma separated value'),
    ('application/drafting', 'application', 'drafting', '.drw', 'MATRA Prelude drafting'),
    ('application/dxf', 'application', 'dxf', '.dxf', 'DXF (AutoCAD)'),
    ('application/filemaker', 'application', 'filemaker', '.fm', 'Filemaker Pro'),
    ('application/futuresplash', 'application', 'futuresplash', '.spl', 'Macromedia Futuresplash'),
    ('application/hdf', 'application', 'hdf', '.hdf', 'NCSA HDF data format'),
    ('application/iges', 'application', 'iges', '.iges', 'Image - IGES graphics format'),
    ('application/mac-binhex40', 'application', 'mac-binhex40', '.hqx', 'Mac binhex 4.0'),
    ('application/mac-compactpro', 'application', 'mac-compactpro', '.cpt', 'Mac Compactpro'),
    ('application/msword', 'application', 'msword', '.doc', 'Microsoft Word'),
    ('application/octet-stream', 'application', 'octet-stream', '.bin', 'Uninterpreted binary'),
    ('application/oda', 'application', 'oda', '.oda', 'ODA ODIF'),
    ('application/pdf', 'application', 'pdf', '.pdf', 'PDF'),
    ('application/postscript', 'application', 'postscript', '.ps', 'PostScript'),
    ('application/rtf', 'application', 'rtf', '.rtf', 'RTF - Rich Text Format'),
    ('application/sla', 'application', 'sla', '.stl', 'Stereolithography'),
    ('text/vcard', 'text', 'vcard', '.vcf', 'VCard'),
    ('application/vda', 'application', 'vda', '.vda', 'VDA-FS Surface data'),
    ('application/vnd.koan', 'application', 'vnd.koan', '.skp', 'SSOYE Koan Files'),
    ('application/vnd.mif', 'application', 'vnd.mif', '.mif', 'FrameMaker MIF format'),
    ('application/vnd.ms-access', 'application', 'vnd.ms-access', '.mdb', 'Microsoft Access file'),
    ('application/vnd.ms-excel', 'application', 'vnd.ms-excel', '.xls', 'Microsoft Excel'),
    ('application/vnd.ms-powerpoint', 'application', 'vnd.ms-powerpoint', '.ppt', 'Microsoft PowerPoint'),
    ('application/vnd.ms-project', 'application', 'vnd.ms-project', '.mpp', 'Microsoft Project'),
    ('application/vnd.wap.wmlc', 'application', 'vnd.wap.wmlc', '.wmlc', 'WML XML in binary format'),
    ('application/vnd.wap.wmlscriptc', 'application', 'vnd.wap.wmlscriptc', '.wmlsc', 'WMLScript bytecode'),
    ('application/vnd.xara', 'application', 'vnd.xara', '.xar', 'CorelXARA'),
    ('application/wordperfect', 'application', 'wordperfect', '.wpd', 'WordPerfect'),
    ('application/wordperfect6.0', 'application', 'wordperfect6.0', '.w60', 'WordPerfect 6.0'),
    ('application/x-arj-compressed', 'application', 'x-arj-compressed', '.arj', 'Archive ARJ '),
    ('application/x-authorware-bin', 'application', 'x-authorware-bin', '.aab', 'Macromedia Authorware'),
    ('application/x-authorware-map', 'application', 'x-authorware-map', '.aam', 'Macromedia Authorware'),
    ('application/x-authorware-seg', 'application', 'x-authorware-seg', '.aas', 'Macromedia Authorware'),
    ('application/x-cdf', 'application', 'x-cdf', '.cdf', 'Channel Definition'),
    ('application/x-cdlink', 'application', 'x-cdlink', '.vcd', 'VCD'),
    ('application/x-chess-pgn', 'application', 'x-chess-pgn', '.pgn', 'Chess PGN file'),
    ('application/x-compress', 'application', 'x-compress', '.z', 'Archive compres'),
    ('application/x-cpio', 'application', 'x-cpio', '.cpio', 'Archive CPIO'),
    ('application/x-csh', 'application', 'x-csh', '.csh', 'C-shell script'),
    ('application/x-debian-package', 'application', 'x-debian-package', '.deb', 'Archive Debian Package'),
    ('application/x-director', 'application', 'x-director', '.dxr', 'Macromedia Director'),
    ('application/x-dvi', 'application', 'x-dvi', '.dvi', 'TeX DVI file'),
    ('application/x-gtar', 'application', 'x-gtar', '.gtar', 'Archive GNU Tar'),
    ('application/gzip', 'application', 'gzip', '.gz', 'Archive gzip compressed'),
    ('application/x-httpd-cgi', 'application', 'x-httpd-cgi', '.cgi', 'CGI Script'),
    ('application/x-illustrator', 'application', 'x-illustrator', '.ai', 'Adobe Illustrator'),
    ('application/x-installshield', 'application', 'x-installshield', '.wis', 'Installshield data'),
    ('application/x-java-jnlp-file', 'application', 'x-java-jnlp-file', '.jnlp', 'Java Network Launching Protocol'),
    ('application/javascript', 'application', 'javascript', '.js', 'Javascript'),
    ('application/x-latex', 'application', 'x-latex', '.latex', 'LaTeX source'),
    ('application/x-ms-wmd', 'application', 'x-ms-wmd', '.wmd', 'Windows Media Services (wmd)'),
    ('application/x-ms-wmz', 'application', 'x-ms-wmz', '.wmz', 'Windows Media Services (wmz)'),
    ('application/x-netcdf', 'application', 'x-netcdf', '.cdf', 'Unidata netCDF'),
    ('application/x-ogg', 'application', 'x-ogg', '.ogg', 'Audio Ogg Vorbis'),
    ('application/x-pagemaker', 'application', 'x-pagemaker', '.p65', 'Adobe PageMaker'),
    ('application/x-photoshop', 'application', 'x-photoshop', '.psd', 'Photoshop'),
    ('application/x-pilot', 'application', 'x-pilot', '.prc', 'Palm Pilot Data'),
    ('application/x-pn-realmedia', 'application', 'x-pn-realmedia', '.rp', 'Audio Real'),
    ('application/x-quattro-pro', 'application', 'x-quattro-pro', '.wq1', 'Quattro Pro'),
    ('application/x-rar-compressed', 'application', 'x-rar-compressed', '.rar', 'Archive RAR'),
    ('application/sdp', 'application', 'sdp', '.sdp', 'Session Description Protocol'),
    ('application/vnd.adobe.flash-movie', 'application', 'vnd.adobe.flash-movie', '.swf', 'Macromedia Shockwave'),
    ('application/x-sql', 'application', 'x-sql', '.sql', 'SQL'),
    ('application/x-stuffit', 'application', 'x-stuffit', '.sit', 'Archive Mac Stuffit compressed'),
    ('application/x-sv4cpio', 'application', 'x-sv4cpio', '.sv4cpio', 'Archive SVR4 cpio'),
    ('application/x-sv4crc', 'application', 'x-sv4crc', '.sv4crc', 'Archive SVR4 crc'),
    ('application/x-tar', 'application', 'x-tar', '.tar', 'Archive Tar'),
    ('application/x-tex', 'application', 'x-tex', '.tex', 'Text - TeX source'),
    ('application/x-texinfo', 'application', 'x-texinfo', '.texinfo', 'Text - Texinfo (emacs)'),
    ('application/x-troff', 'application', 'x-troff', '.tr', 'Text - troff'),
    ('application/x-troff-man', 'application', 'x-troff-man', '.man', 'Text - troff with MAN macros'),
    ('application/x-troff-me', 'application', 'x-troff-me', '.me', 'Text - troff with ME macros'),
    ('application/x-troff-ms', 'application', 'x-troff-ms', '.ms', 'Text - troff with MS macros'),
    ('application/x-ustar', 'application', 'x-ustar', '.ustar', 'Archive POSIX Tar'),
    ('application/x-x509-ca-cert', 'application', 'x-x509-ca-cert', '.cacert', 'X509 CA Cert'),
    ('application/zip', 'application', 'zip', '.zip', 'Archive Zip'),
    ('audio/basic', 'audio', 'basic', '.au', 'Basic audio (m-law PCM)'),
    ('audio/midi', 'audio', 'midi', '.midi', 'Audio Midi'),
    ('audio/x-mpeg', 'audio', 'x-mpeg', '.mp3', 'Audio MPEG'),
    ('audio/x-mpeg2', 'audio', 'x-mpeg2', '.mp2a', 'Audio MPEG-2'),
    ('audio/rmf', 'audio', 'rmf', '.rmf', 'Audio Java Media Framework'),
    ('audio/voice', 'audio', 'voice', '.voc', 'Audio Voice'),
    ('audio/x-aiff', 'audio', 'x-aiff', '.aif', 'Audio AIFF'),
    ('audio/x-mod', 'audio', 'x-mod', '.xm', 'Audio Mod'),
    ('audio/x-mpegurl', 'audio', 'x-mpegurl', '.m3u', 'Audio mpeg url (m3u)'),
    ('audio/x-ms-wma', 'audio', 'x-ms-wma', '.wma', 'Audio Windows Media Services (wma)'),
    ('audio/x-ms-wmv', 'audio', 'x-ms-wmv', '.wmv', 'Audio Windows Media Services (wmv)'),
    ('audio/x-pn-realaudio', 'audio', 'x-pn-realaudio', '.ra', 'Audio Realaudio'),
    ('audio/x-pn-realaudio-plugin', 'audio', 'x-pn-realaudio-plugin', '.rm', 'Audio Realaudio Plugin'),
    ('audio/x-wav', 'audio', 'x-wav', '.wav', 'Audio Microsoft WAVE'),
    ('chemical/x-pdb', 'chemical', 'x-pdb', '.pdb', 'Chemical Brookhaven PDB'),
    ('chemical/x-xyz', 'chemical', 'x-xyz', '.xyz', 'Chemical XMol XYZ'),
    ('drawing/x-dwf', 'drawing', 'x-dwf', '.dwf', 'WHIP Web Drawing file'),
    ('image/bmp', 'image', 'bmp', '.bmp', 'Image - BMP'),
    ('image/fif', 'image', 'fif', '.fif', 'Image - Fractal Image Format'),
    ('image/gif', 'image', 'gif', '.gif', 'Image - Gif'),
    ('image/ief', 'image', 'ief', '.ief', 'Image - Image Exchange Format'),
    ('image/jpeg', 'image', 'jpeg', '.jpg', 'Image - Jpeg'),
    ('image/png', 'image', 'png', '.png', 'Image - PNG'),
    ('image/heic', 'image', 'heic', '.heic', 'Image - HEIC'),
    ('image/heic-sequence', 'image', 'heic-sequence', '.heics', 'Image - HEIC sequence'),
    ('image/heif', 'image', 'heif', '.heif', 'Image - HEIF'),
    ('image/heif-sequence', 'image', 'heif-sequence', '.heifs', 'Image - HEIF sequence'),
    ('image/tiff', 'image', 'tiff', '.tif', 'Image - TIFF'),
    ('image/vnd.wap.wbmp', 'image', 'vnd.wap.wbmp', '.wbmp', 'Image - WAP wireless bitmap'),
    ('image/x-cmu-raster', 'image', 'x-cmu-raster', '.ras', 'Image - CMU Raster'),
    ('image/x-fits', 'image', 'x-fits', '.fit', 'Image - Flexible Image Transport'),
    ('image/x-freehand', 'image', 'x-freehand', '.fh', 'Image - Macromedia Freehand'),
    ('image/svg+xml', 'image', 'svg+xml', '.svg', 'Image - SVG'),
    ('image/x-photo-cd', 'image', 'x-photo-cd', '.pcd', 'Image - PhotoCD'),
    ('image/x-pict', 'image', 'x-pict', '.pict', 'Image - Mac pict'),
    ('image/x-portable-anymap', 'image', 'x-portable-anymap', '.pnm', 'Image - PNM'),
    ('image/x-portable-bitmap', 'image', 'x-portable-bitmap', '.pbm', 'Image - PBM'),
    ('image/x-portable-graymap', 'image', 'x-portable-graymap', '.pgm', 'Image - PGM'),
    ('image/x-portable-pixmap', 'image', 'x-portable-pixmap', '.ppm', 'Image - Portable Pixmap'),
    ('image/x-rgb', 'image', 'x-rgb', '.rgb', 'Image - RGB'),
    ('image/x-xbitmap', 'image', 'x-xbitmap', '.xbm', 'Image - X bitmap'),
    ('image/x-xpixmap', 'image', 'x-xpixmap', '.xpm', 'Image - X pixmap'),
    ('image/x-xwindowdump', 'image', 'x-xwindowdump', '.xwd', 'Image - X window dump (xwd)'),
    ('message/rfc822', 'message', 'rfc822', '.mime', 'RFC822 Message'),
    ('model/mesh', 'model', 'mesh', '.mesh', 'Computational mesh'),
    ('text/sgml', 'text', 'sgml', '.sgml', 'Text - SGML Text'),
    ('text/css', 'text', 'css', '.css', 'Text - CSS'),
    ('text/html', 'text', 'html', '.html', 'Text - HTML'),
    ('text/plain', 'text', 'plain', '.txt', 'Text - Plain text'),
    ('text/plain; format=flowed', 'text', 'plain; format=flowed', '.text', 'Text - Plain text (flowed)'),
    ('text/enriched', 'text', 'enriched', '.rtx', 'Text - Enriched Text'),
    ('text/tab-separated-values', 'text', 'tab-separated-values', '.tsv', 'Text - Tab separated values'),
    ('text/vnd.wap.wmlscript', 'text', 'vnd.wap.wmlscript', '.wmls', 'Text - WMLScript'),
    ('text/vnd.wap.wml', 'text', 'vnd.wap.wml', '.wml', 'Text - WML'),
    ('text/xml', 'text', 'xml', '.xml', 'Text - XML Document'),
    ('text/x-setext', 'text', 'x-setext', '.etx', 'Text - Structured enhanced text'),
    ('text/xsl', 'text', 'xsl', '.xsl', 'Text - XSL'),
    ('video/fli', 'video', 'fli', '.fli', 'Video FLI'),
    ('video/x-flv', 'video', 'x-flv', '.flv', 'Video FLASH'),
    ('video/mpeg', 'video', 'mpeg', '.mpg', 'Video MPEG'),
    ('video/mpeg2', 'video', 'mpeg2', '.mpv2', 'Video MPEG-2'),
    ('video/quicktime', 'video', 'quicktime', '.mov', 'Video Quicktime'),
    ('video/vdo', 'video', 'vdo', '.vdo', 'Video VDOlive streaming'),
    ('video/vnd.vivo', 'video', 'vnd.vivo', '.vivo', 'Video Vivo'),
    ('video/x-ms-asf', 'video', 'x-ms-asf', '.asf', 'Video Microsoft ASF'),
    ('video/x-ms-wm', 'video', 'x-ms-wm', '.wm', 'Video Windows Media Services (wm)'),
    ('video/x-ms-wvx', 'video', 'x-ms-wvx', '.wvx', 'Video Windows Media Services (wvx)'),
    ('video/x-mx-wmx', 'video', 'x-mx-wmx', '.wmx', 'Video Windows Media Services (wmx)'),
    ('video/x-msvideo', 'video', 'x-msvideo', '.avi', 'Video Microsoft AVI'),
    ('video/x-sgi-movie', 'video', 'x-sgi-movie', '.movie', 'Video SGI movie player'),
    ('x-conference/x-cooltalk', 'x-conference', 'x-cooltalk', '.ice', 'Conference Cooltalk'),
    ('x-world/x-vrml', 'x-world', 'x-vrml', '.vrml', 'VRML'),
    ('xuda/gen-cert', 'xuda', 'gen-cert', '.xuda', 'Xuda'),
    ('text/enhanced', 'text', 'enhanced', '.etxt', 'Enhanced text'),
    ('text/markdown', 'text', 'markdown', '.mtxt', 'Markdown text'),
    ('text/fixed-width', 'text', 'fixed-width', '.ftxt', 'Fixed-width text'),
    ('application/vnd.sun.xml.calc', 'application', 'vnd.sun.xml.calc', '.sxc', 'OpenOffice Spreadsheet'),
    ('application/vnd.sun.xml.calc.template', 'application', 'vnd.sun.xml.calc.template', '.stc', 'OpenOffice Spreadsheet Template'),
    ('application/vnd.sun.xml.draw', 'application', 'vnd.sun.xml.draw', '.sxd', 'OpenOffice Draw'),
    ('application/vnd.sun.xml.draw.template', 'application', 'vnd.sun.xml.draw.template', '.std', 'OpenOffice Draw Template'),
    ('application/vnd.sun.xml.impress', 'application', 'vnd.sun.xml.impress', '.sxi', 'OpenOffice Impress'),
    ('application/vnd.sun.xml.impress.template', 'application', 'vnd.sun.xml.impress.template', '.sti', 'OpenOffice Impress Template'),
    ('application/vnd.sun.xml.math', 'application', 'vnd.sun.xml.math', '.sxm', 'OpenOffice Math'),
    ('application/vnd.sun.xml.writer', 'application', 'vnd.sun.xml.writer', '.sxw', 'OpenOffice Writer'),
    ('application/vnd.sun.xml.writer.global', 'application', 'vnd.sun.xml.writer.global', '.sxg', 'OpenOffice Writer Global'),
    ('application/vnd.sun.xml.writer.template', 'application', 'vnd.sun.xml.writer.template', '.stw', 'OpenOffice Writer Template'),
    ('audio/wav', 'audio', 'wav', '.wav', 'Audio - WAV'),
    ('audio/mpeg', 'audio', 'mpeg', '.mpeg', 'Audio - MPEG'),
    ('audio/mp3', 'audio', 'mp3', '.mp3', 'Audio - MP3'),
    ('image/pjpeg', 'image', 'pjpeg', '.pjpeg', 'Image - Progressive JPEG'),
    ('application/x-spss-savefile', 'application', 'x-spss-savefile', '.sav', 'SPPS data file'),
    ('application/x-spss-outputfile', 'application', 'x-spss-outputfile', '.spo', 'SPPS data file'),
    ('video/mp4', 'video', 'mp4', '.mp4', 'Video MP4'),
    ('application/x-xpinstall', 'application', 'x-xpinstall', '.xpi', 'XPInstall'),
    ('text/calendar', 'text', 'calendar', '.ics', 'iCalendar'),
    ('application/vnd.microsoft.portable-executable', 'application', 'vnd.microsoft.portable-executable', '.exe', 'Microsoft Portable Executable'),
    ('model/vnd.mts', 'model', 'vnd.mts', '.mts', 'Virtue MTS'),
    ('image/vnd.ms-modi', 'image', 'vnd.ms-modi', '.mdi', 'Microsoft Document Imaging Format'),
    ('application/wsdl+xml', 'application', 'wsdl+xml', '.wsdl', 'WSDL - Web Services Description Language'),
    ('multipart/voice-message', 'multipart', 'voice-message', '.vpm', 'VPIM voice message'),
    ('application/vnd.wolfram.player', 'application', 'vnd.wolfram.player', '.nbp', 'Mathematica Notebook Player'),
    ('application/vnd.smart.notebook', 'application', 'vnd.smart.notebook', '.notebook', 'SMART Notebook'),
    ('application/vnd.novadigm.ext', 'application', 'vnd.novadigm.ext', '.ext', 'Novadigm RADIA and EDM products'),
    ('application/vnd.novadigm.edx', 'application', 'vnd.novadigm.edx', '.edx', 'Novadigm RADIA and EDM products'),
    ('application/vnd.ms-xpsdocument', 'application', 'vnd.ms-xpsdocument', '.xps', 'Microsoft XML Paper Specification'),
    ('application/vnd.ms-wpl', 'application', 'vnd.ms-wpl', '.wpl', 'Microsoft Windows Media Player Playlist'),
    ('application/vnd.ms-officetheme', 'application', 'vnd.ms-officetheme', '.thmx', 'Microsoft Office System Release Theme'),
    ('application/vnd.lotus-wordpro', 'application', 'vnd.lotus-wordpro', '.lwp', 'Lotus Wordpro'),
    ('application/vnd.geogebra.file', 'application', 'vnd.geogebra.file', '.ggb', 'GeoGebra'),
    ('application/vnd.fdf', 'application', 'vnd.fdf', '.fdf', 'Forms Data Format'),
    ('application/solids', 'application', 'solids', '.sol', 'Solids'),
    ('application/smil+xml', 'application', 'smil+xml', '.smi', 'Synchronized Multimedia Integration Language'),
    ('application/pkcs10', 'application', 'pkcs10', '.p', 'PKCS #10 - Certification Request Standard'),
    ('application/oxps', 'application', 'oxps', '.oxps', 'OpenXPS'),
    ('application/mathematica', 'application', 'mathematica', '.nb', 'Mathematica Notebooks'),
    ('application/base64', 'application', 'base64', '.mm', ''),
    ('application/vnd.biopax.rdf+xml', 'application', 'vnd.biopax.rdf+xml', '.owl', 'BioPAX OWL'),
    ('application/vnd.tcpdump.pcap', 'application', 'vnd.tcpdump.pcap', '.pcap', 'Tcpdump Packet Capture'),
    ('application/vnd.oasis.opendocument.text', 'application', 'vnd.oasis.opendocument.text', '.odt', 'OpenDocument Text'),
    ('application/vnd.oasis.opendocument.text-template', 'application', 'vnd.oasis.opendocument.text-template', '.ott', 'OpenDocument Text Template'),
    ('application/vnd.oasis.opendocument.text-web', 'application', 'vnd.oasis.opendocument.text-web', '.oth', 'HTML Document Template'),
    ('application/vnd.oasis.opendocument.text-master', 'application', 'vnd.oasis.opendocument.text-master', '.odm', 'OpenDocument Master Document'),
    ('application/vnd.oasis.opendocument.graphics', 'application', 'vnd.oasis.opendocument.graphics', '.odg', 'OpenDocument Drawing'),
    ('application/vnd.oasis.opendocument.graphics-template', 'application', 'vnd.oasis.opendocument.graphics-template', '.otg', 'OpenDocument Drawing Template'),
    ('application/vnd.oasis.opendocument.presentation', 'application', 'vnd.oasis.opendocument.presentation', '.odp', 'OpenDocument Presentation'),
    ('application/vnd.oasis.opendocument.presentation-template', 'application', 'vnd.oasis.opendocument.presentation-template', '.otp', 'OpenDocument Presentation Template'),
    ('application/vnd.oasis.opendocument.spreadsheet', 'application', 'vnd.oasis.opendocument.spreadsheet', '.ods', 'OpenDocument Spreadsheet'),
    ('application/vnd.oasis.opendocument.spreadsheet-template', 'application', 'vnd.oasis.opendocument.spreadsheet-template', '.ots', 'OpenDocument Spreadsheet Template'),
    ('application/vnd.oasis.opendocument.chart', 'application', 'vnd.oasis.opendocument.chart', '.odc', 'OpenDocument Chart'),
    ('application/vnd.oasis.opendocument.formula', 'application', 'vnd.oasis.opendocument.formula', '.odf', 'OpenDocument Formula'),
    ('application/vnd.oasis.opendocument.database', 'application', 'vnd.oasis.opendocument.database', '.odb', 'OpenDocument Database'),
    ('application/vnd.oasis.opendocument.image', 'application', 'vnd.oasis.opendocument.image', '.odi', 'OpenDocument Image'),
    ('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet', '.xlsx', 'Microsoft Office Excel'),
    ('application/vnd.openxmlformats-officedocument.spreadsheetml-template', 'application', 'vnd.openxmlformats-officedocument.spreadsheetml-template', '.xltx', 'Microsoft Office Excel Template'),
    ('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application', 'vnd.openxmlformats-officedocument.presentationml.presentation', '.pptx', 'Microsoft Office PowerPoint Presentation'),
    ('application/vnd.openxmlformats-officedocument.presentationml.slideshow', 'application', 'vnd.openxmlformats-officedocument.presentationml.slideshow', '.ppsx', 'Microsoft Office PowerPoint Slideshow'),
    ('application/vnd.openxmlformats-officedocument.presentationml-template', 'application', 'vnd.openxmlformats-officedocument.presentationml-template', '.potx', 'Microsoft Office PowerPoint Template'),
    ('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application', 'vnd.openxmlformats-officedocument.wordprocessingml.document', '.docx', 'Microsoft Office Word'),
    ('application/vnd.openxmlformats-officedocument.wordprocessingml-template', 'application', 'vnd.openxmlformats-officedocument.wordprocessingml-template', '.dotx', 'Microsoft Office Word Template');

